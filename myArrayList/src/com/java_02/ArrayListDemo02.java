package com.java_02;

import java.util.ArrayList;

/*
    几何的常用方法：删除、修改、返回指定索引处元素、返回元素个数
 */
public class ArrayListDemo02 {
    public static void main(String[] args) {
        // 创建集合，并添加元素
        ArrayList<String> array = new ArrayList<>();
        array.add("hello");
        array.add("world");
        array.add("liqinz");
        // public boolean remove(Object o), 删除指定的元素，返回删除是否成功
        //System.out.println(array.remove("liqinz"));

        // public E remove(int index),删除指定索引处的元素，返回被删除的元素
        //System.out.println(array.remove(1));

        // public E set(int index, E element), 修改指定索引处的元素，返回被修改的元素
        //System.out.println(array.set(1,"java"));

        // public E get(int index), 返回指定索引处的元素
        //System.out.println(array.get(1));
       // System.out.println(array.get(5));           IndexOutOfBoundsException

        // public int size(), 返回集合中元素的个数
        System.out.println(array.size());

        System.out.println(array);
    }

}
