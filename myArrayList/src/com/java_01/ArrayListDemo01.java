package com.java_01;

import java.util.ArrayList;

/*
    集合的创建与元素的添加
 */
public class ArrayListDemo01 {
    public static void main(String[] args) {
        //　public ArrayList(), 创建一个空的几何对象
        ArrayList<String> array = new ArrayList<String>();

        // public boolean add(E e), 将指定的元素追加到此几何的末尾
        array.add("w");
        array.add("rui");
        array.add("pu");
        // public void add(int index, E element), 在此集合的指定位置插入指定元素
        array.add(0,"li");
        array.add(3,"520");

        //[li, w, rui, 520, pu]

        // 输出结合
        System.out.println(array);
        // array.add(6,"fale");//IndexOutOfBoundsException 索引超出返回报错

    }
}
