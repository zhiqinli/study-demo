package com.java_04;
// 定义学生对象

public class Student {
    // 成员变量
    private String name;
    private int age;

    // 构造方法
    public Student() {}
    public Student(String name, int age){
        this.name = name;
        this.age = age;
    }

    // 获取改变成员变量的方法
    public String getName(){
        return name;
    }
    public int getAge(){
        return age;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setAge(int age){
        this.age = age;
    }

}
