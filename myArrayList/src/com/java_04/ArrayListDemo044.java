package com.java_04;

import com.java_05.Student;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListDemo044 {
    public static void main(String[] args) {
        // 创建集合对象
        ArrayList<Student> arr = new ArrayList<>();

        // 创建键盘录入对象
        Scanner sc = new Scanner(System.in);
        String name ;
        String age ;

        for (int i = 1; i < 4; i++){
            System.out.println("请输入学生的姓名和年龄");
            name = sc.nextLine();
            age = sc.nextLine();
            Student s = new Student(name, age);
            arr.add(s);
        }

        for (int i = 0; i < arr.size(); i++){
            Student s = arr.get(i);
            System.out.println(s.getName() + "," + s.getAge());
        }
    }
}
