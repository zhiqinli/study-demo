package com.java_04;

import java.util.ArrayList;

/*
    存储学生对象并遍历
 */
public class ArrayListDemo04 {
    public static void main(String[] args) {
        // 创建集合对象
        ArrayList<Student> arr = new ArrayList<>();

        // 创建学生对象
        Student st1 = new Student("liqinzhi", 20);
        Student st2 = new Student("wangruiping", 22);
        Student st3 = new Student("lipeixun", 9);

        // 将学生对象添加到集合中
        arr.add(st1);
        arr.add(st2);
        arr.add(st3);

        // 遍历集合
        for (int i = 0; i < arr.size(); i++) {
            Student s = arr.get(i);
            System.out.println(s.getName() + "," + s.getAge());
        }
    }
}
