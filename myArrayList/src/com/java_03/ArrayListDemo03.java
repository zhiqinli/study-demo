package com.java_03;

import java.util.ArrayList;

/*
    用集合存储字符串并遍历
 */
public class ArrayListDemo03 {
    public static void main(String[] args) {
        // 创建集合对象
        ArrayList<String> arr = new ArrayList<>();

        // 添加集合元素
        arr.add("wang");
        arr.add("rui");
        arr.add("ping");

        // 遍历集合通用格式（获取每一个元素和集合长度）
        for (int i = 0; i < arr.size(); i++){
            String s = arr.get(i);
            System.out.println(s);

            // 数组是arr.length;arr[i]
        }

    }
}
