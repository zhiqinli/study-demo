package com.java_05;

import java.util.ArrayList;
import java.util.Scanner;

/*
    存储学生对象并遍历录入其属性
 */
public class ArrayListDemo05 {

    public static void main(String[] args) {
        // 创建集合对象
        ArrayList<Student> arr = new ArrayList<>();

        // 创建键盘录入对象
        Scanner sc = new Scanner(System.in);

       /*// 创建学生对象，将键盘录入值赋值给学生对象
        System.out.println("请输入第一个学生的姓名和年龄");
        String name = sc.nextLine();
        String age = sc.nextLine();
        Student st1 = new Student(name, age);

        System.out.println("请输入第二个学生的姓名和年龄");
        name = sc.nextLine();
        age = sc.nextLine();
        Student st2 = new Student(name, age);

        System.out.println("请输入第三个学生的姓名和年龄");
        name = sc.nextLine();
        age = sc.nextLine();
        Student st3 = new Student(name, age);

        // 往集合中添加学生对象
        arr.add(st1);
        arr.add(st2);
        arr.add(st3);*/
        // 调用添加学生函数
        addSrting(arr);
        addSrting(arr);

        // 遍历集合
        for (int i = 0; i < arr.size(); i++){
            Student s = arr.get(i);
            System.out.println(s.getName() + "," + s.getAge());
        }
    }

    // 将学生对象传到指定集合中
    public static void addSrting(ArrayList<Student> arr){
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入学生的姓名和年龄");
        String name = sc.nextLine();
        String age = sc.nextLine();
        Student st = new Student(name, age);
        arr.add(st);
    }

}
