package com.java_05;
/*
     创建学生类
 */
public class Student {
    // 成员
    private String name;
    private String age;

    // 构造方法
    public Student() {}
    public Student(String name, String age){
        this.age = age;
        this.name = name;
    }

    //获取成员变量的方法
    public String getName(){
        return name;
    }
    public String getAge(){
        return age;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setAge(String age){
        this.age = age;
    }


}
