package com.java_01;
/*
    抽象类、抽象方法需用abstract修饰
    抽象类中可以没有抽象方法，但是有抽象方法的类一定是抽象类
    抽象类不能实例化，只能通过子类对象实例化（抽象类多态）
    抽象类的子类：重写抽象类中的所有抽象方法or是抽象类
 */
public class AnimalDemo {
    public static void main(String[] args) {
        //Animal a = new Animal() ;     抽象类不是具体的不能new创建对象

        // 通过多态形式，子类对象来实例化
        Animal a = new Cat();
        a.eat();
        a.sleep();


    }

}
