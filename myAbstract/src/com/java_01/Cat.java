package com.java_01;

public class Cat extends Animal{
    @Override
    // 重写了抽象类中所有的抽象方法
    public void eat() {
        System.out.println("猫吃鱼");
    }
}
