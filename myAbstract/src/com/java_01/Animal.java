package com.java_01;
/*
    抽象类
 */
public  abstract class Animal { // 一个类中有抽象方法，类也必须定义为抽象的；但是抽象类中可以没有抽象方法

    // 抽象方法
    public abstract void eat();

    // 具体方法
    public void sleep(){
        System.out.println("睡觉");
    }
}
