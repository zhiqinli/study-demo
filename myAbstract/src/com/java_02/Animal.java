package com.java_02;
/*
    抽象类
 */
public abstract class Animal {

    // 成员变量
    private int age = 20;
    private final String city = "背景";

    // 构造方法
    // 抽象类虽然不能直接实例化，但是可以使用多态的方式，通过子类实例化。
    // 构造方法：用于子类访问父类数据的初始化

    public Animal(){}

    public Animal(int age){
        this.age = age;
    }



    // 非抽象成员方法，可以提高代码的复用性
    public void show(){
        age = 40;
        System.out.println(age);
        //city = "南充";
        System.out.println(city);
    }
    // 抽象成员方法，限定子类必须完成某些动作
    public abstract void eat();
}
