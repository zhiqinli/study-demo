package com.java_04;
/*
    类和类
        继承，只能单继承，但可以多层继承（世代单传）
    类和接口
        实现，可以单实现，也可以多实现，还可以再继承一个类的同时实现多个接口（独生子技能数不限）
    接口和接口
        继承，可以单继承，也可多继承（一个技能可源自多个技能，也可传给多个技能）
 */
public class Demo {
    public static void main(String[] args) {
        // 创建对象
        BasktSpo bs = new BasktSpo("蓝云",20);
        BasktTeac bt = new BasktTeac("懒觉",30);
        PinSpo ps = new PinSpo("平云",18);
        PinTeac pt = new PinTeac("平角",50);

        // 篮球运动员
        System.out.println(bs.getName() + bs.getAge());
        bs.eat();
        bs.learn();
        System.out.println("-------------");

        // 篮球教练
        System.out.println(bt.getName() + bt.getAge());
        bt.eat();
        bt.teach();
        System.out.println("-------------");

        // 乒乓球运动员
        System.out.println(ps.getName() + ps.getAge());
        ps.eat();
        ps.learn();
        ps.learnEng();
        System.out.println("-------------");

        // 乒乓球教练
        System.out.println(pt.getName() + pt.getAge());
        pt.eat();
        pt.teach();
        pt.learnEng();
        System.out.println("-------------");

    }
}
