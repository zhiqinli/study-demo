package com.java_04;

public class PinSpo extends Sporter implements ILearnEng{
    public PinSpo() {
    }

    public PinSpo(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("乒乓球运动员吃营养粉");
    }

    @Override
    public void learnEng() {
        System.out.println("乒乓球运动员学英语");
    }

    @Override
    public void learn() {
        System.out.println("乒乓球运动员学乒乓球");
    }
}
