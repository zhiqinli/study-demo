package com.java_04;

public abstract class Sporter extends People{
    public Sporter() {
    }

    public Sporter(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("运动员吃营养品");
    }

    public abstract void learn();
}
