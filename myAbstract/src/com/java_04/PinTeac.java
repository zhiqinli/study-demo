package com.java_04;

public class PinTeac extends Teacher implements ILearnEng{
    public PinTeac() {
    }

    public PinTeac(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("乒乓球教练吃饭");
    }

    @Override
    public void learnEng() {
        System.out.println("乒乓球教练学习英语");
    }

    @Override
    public void teach() {
        System.out.println("乒乓球教练教乒乓球");
    }
}
