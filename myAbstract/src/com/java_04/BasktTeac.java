package com.java_04;

public class BasktTeac extends Teacher{
    public BasktTeac() {
    }

    public BasktTeac(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("篮球教练吃饭");
    }


    @Override
    public void teach() {
        System.out.println("篮球教练教篮球");
    }
}
