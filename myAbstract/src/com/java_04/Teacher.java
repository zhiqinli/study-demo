package com.java_04;

public abstract class Teacher extends People{

    public Teacher() {
    }

    public Teacher(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("教练喝饮料");
    }

    public abstract void teach();
}
