package com.wrp.socket.demo1;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 模拟服务端
 * 功能：根据边长计算正方形的面积
 */
public class SocketServer {
    public static void main(String[] args) throws IOException {
        int port = 7000;
        // 创建套接字
        ServerSocket serverSocket = new ServerSocket(port);
        // 监听连接
        Socket socket = serverSocket.accept();

        DataInputStream dis = new DataInputStream(new BufferedInputStream(
                socket.getInputStream()));
        DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(
                socket.getOutputStream()));

        do {
            double length = dis.readDouble();
            System.out.println("服务器端收到的边长数据为：" + length);
            double result = length * length;
            dos.writeDouble(result);
            dos.flush();
        } while (dis.readInt() != 0);

        // 关闭套接字
        socket.close();
        serverSocket.close();
    }
}
