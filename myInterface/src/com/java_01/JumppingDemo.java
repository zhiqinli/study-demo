package com.java_01;
/*
    关键字：interface
    类实现接口用implements
    接口实例化采用多态 形式，用接口实现类对象进行实例化
    多态的前提：有继承或者实现关系，有方法重写；有父（类/接口）引用指向（子/实现）类对象
    接口的实现类：重写接口中的所有抽象方法/是抽象类
 */
public class JumppingDemo {
    public static void main(String[] args) {
        //Jumpping' is abstract; cannot be instantiated   接口也是一个抽象的内容
       // Jumpping j = new Jumpping();

        // 不能直接实例化，只能通过多态的方式实例化
        Jumpping j = new Cat();
        j.jump();
    }
}
