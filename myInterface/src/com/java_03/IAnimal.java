package com.java_03;
/*
        接口
 */
public interface IAnimal {
    // 方法必须是抽象类，即没有方法体
    void jump();    // public abstract

}
