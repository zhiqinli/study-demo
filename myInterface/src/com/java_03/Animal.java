package com.java_03;
/*
        抽象动物类
 */

public abstract class Animal {
    private String name;
    private int age;

    // 抽象类虽然不能通过构造方法直接创建对象，但是可以用其继承子类的构造方法创建
    public Animal() {
    }

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // 吃饭抽象成员方法
    public abstract void eat();
}
