package com.java_03;

public class Cat extends Animal implements IAnimal{

    public Cat() {
    }

    public Cat(String name, int age) {
        super(name, age);
    }

    // 其继承接口的所有方法（都是抽象方法）需要重写
    @Override
    public void jump() {
        System.out.println("猫会跳房子");
    }

    // // 其继承接口的抽象方法需要重写
    @Override
    public void eat() {
        System.out.println("猫要吃鱼");
    }
}
