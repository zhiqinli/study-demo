package com.java_03;

public class Dog extends Animal implements IAnimal{
    public Dog() {
    }

    public Dog(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("狗会跳楼梯");
    }

    @Override
    public void jump() {
        System.out.println("狗吃骨头");
    }
}
