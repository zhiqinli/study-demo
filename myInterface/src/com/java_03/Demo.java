package com.java_03;

public class Demo {
    public static void main(String[] args) {
        // 接口实例,接口注重行为
        IAnimal ia = new Cat();
        ia.jump();      // 只能使用接口中存在的方法

        // 抽象类实例
        //Animal a = new Cat();
        Animal a = (Animal) ia;
        a.setName("英短");
        a.setAge(3);
        a.eat();    // 只能使用抽象类中存在的方法
        System.out.println(a.getName() + a.getAge());


        //具体子类中有继承实现的所有方法
       Dog d = new Dog("吉娃娃" ,7);
        System.out.println(d.getName() + d.getAge());
        d.eat();
        d.jump();

    }
}
