package com.java_02;

public class InterfaceDemo {
    public static void main(String[] args) {
        Inter i = new InterImpl();
        //i.num = 1;
        // 报错，因为接口中成员变量默认都被final修饰，即为成员常量

        System.out.println(i.num);
        System.out.println(i.num2);
        System.out.println(Inter.num);      // 接口名. 变量名
       // i.num2 = 1;  常量不能重新赋值
    }
}
