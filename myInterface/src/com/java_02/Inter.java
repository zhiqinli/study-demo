package com.java_02;

/*
    接口成员的特点
        成员变量
            只能是常量
            默认修饰符public final static
        构造方法
            接口没有构造方法，因为接口是对行为进行抽象，是没有具体存在
            一个类如果没有父类，默认继承自Object
        成员方法
            只能是抽象方法
            默认修饰符：public abstract

          抽象类可以有非抽象方法，变量也可以存在
 */
public interface Inter {
    // 默认被静态修饰，且为常量
    public int num = 10;
    public final int num2 = 20;
    public final static int num3 = 30;  // int num3 = 30


    // 接口没有构造方法，因为接口是对行为进行抽象，所以没有构造方法
    //public inter(){}

    // 接口中不能有非抽象方法
    //public void show(){}

    // 接口中的成员方法只能是抽象的,"public abstract"是默认修饰符
    public abstract void show();        //void show();



}
