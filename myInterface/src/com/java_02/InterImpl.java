package com.java_02;

// 实现类名一般命名为接口名+Impl   意为xx接口的实现
public class InterImpl implements Inter{
    // public class InterImpl extends Object implements Inter
    //Object 所有类的祖宗
    public InterImpl(){
        super();
    }

    @Override
    public void show() {
        System.out.println("show");
    }
}
