package com.java_02;

public class ArrayTest {
    public static void main(String[] args) {
        int[] arr = {11, 22, 33, 44};
        for (int i = 0; i < arr.length; i++){
            System.out.println(arr[i]);
        }
        int min = arr[0];
        for (int x = 1;x < arr.length; x++){
            if (min > arr[x])
                min = arr[x];
        }
        System.out.println("min:" + min);
    }
}
