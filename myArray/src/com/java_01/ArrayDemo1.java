package com.java_01;

public class ArrayDemo1 {
    public static void main(String[] args) {
        int[] arr1 = new int[3];
        arr1[0] = 100;
        arr1[1] = 200;
        arr1[2] = 300;
        System.out.println(arr1);
        System.out.println(arr1[0]);
        System.out.println(arr1[1]);
        System.out.println(arr1[2]);

        int[] arr2 = arr1;
        arr2[0] = 111;
        arr2[1] = 222;
        arr2[2] = 333;

        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(arr1[0]);
        System.out.println(arr2[0]);

        int[] arr3 = {1, 2, 3};
        System.out.println(arr3);
        System.out.println(arr3[0]);
        System.out.println(arr3[1]);
        System.out.println(arr3[2]);

        // 空值，引用数据类型默认值，表示不指向任何有效对象
       arr3 = null;
        System.out.println(arr3);
        //System.out.println(arr3[4]);    //ArrayIndexOutOfBoundsException
        //System.out.println(arr3[2]);      //NullPointerException
    }
}
