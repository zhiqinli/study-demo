package com.java;

import java.util.Scanner;

public class Test08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[5];
        //float grade;

        // 输入评委分数存入数组
        System.out.println("请输入评委分数：");
        for (int i = 0; i < arr.length; i++){
            arr[i] = sc.nextInt();
        }

        // 调用最大最小函数，并求平均数
        int max = getMax(arr);
        int min = getMin(arr);
        int sum = getSum(arr) - max - min;
        float grade = sum/(arr.length - 2);
        System.out.println("最终成绩为：" + grade);

    }
    public static int getMax(int[] arr){
        int max = arr[0];
        for (int i = 1; i < arr.length; i++){
            max = (max > arr[i] ? max : arr[i]);
        }
        //System.out.println(max);
        return max;
    }

    public static int getMin(int[] arr){
        int min = arr[0];
        for (int i = 1; i < arr.length; i++){
            min = (min < arr[i] ? min : arr[i]);
        }
        //System.out.println(min);
        return min;
    }

    public static int getSum(int[] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
        }
        return sum;
    }
}
