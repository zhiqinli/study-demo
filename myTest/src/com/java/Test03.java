package com.java;

public class Test03 {
    public static void main(String[] args) {
        System.out.println(numRab());

    }

    public static int numRab(){
        int[] arr = new int[20];
        arr[0] = 1;
        arr[1] = 1;
        for (int i = 2; i < arr.length; i++){
            arr[i] = arr[i-1] + arr[i-2];
        }
        return arr[19];
    }
}
