package com.java;

public class Test05 {
    public static void main(String[] args) {
        int[] arr1 = {1,2,3,4,5};
        int[] arr2 = {1,2,3,4,6};
        System.out.println(isSame(arr1,arr2));
    }
    public static boolean isSame(int[] arr1, int[] arr2){
        if (arr1.length != arr2.length)
            return false;
        else {
            for (int i = 0; i < arr1.length; i++){
                if (arr1[i] != arr2[i])
                    return false;
            }
            return true;
        }

    }
}
