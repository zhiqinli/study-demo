package com.java;



// alt enter 快捷键导包
// 自动导包

import java.util.Scanner;

public class Test01 {
    public static void main(String[] args) {
        // 创建对象
        Scanner sc = new Scanner(System.in);

        System.out.print("请输入星期数： ");
        int week = sc.nextInt();
        toTo(week);

    }

    public static void toTo(int week){
        switch (week){
            case 1:
                System.out.println("跑步");
                break;
            case 2:
                System.out.println("游泳");
                break;
            case 3:
                System.out.println("慢走");
                break;
            case 4:
                System.out.println("动感单车");
                break;
            case 5:
                System.out.println("拳击");
                break;
            case 6:
                System.out.println("爬山");
                break;
            case 7:
                System.out.println("大吃一顿");
                break;
            default:
                System.out.println("你输入的星期数有误");
                break;
        }

    }

}
