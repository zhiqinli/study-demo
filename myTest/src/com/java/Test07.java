package com.java;

public class Test07 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5};
        int temp;
        for (int i = 0, t = arr1.length - 1; i != t; i++,t--){
           temp = arr1[i];
           arr1[i] = arr1[t];
           arr1[t] = temp;
            }
        for (int i = 0; i < arr1.length; i++){
            System.out.print(arr1[i] + " ");
        }
        }
    }

