package com.java;

import java.util.Scanner;

public class Test06 {
    public static void main(String[] args) {
//        int s = -1;
        int[] arr = {23, 86, 56, 29, 48};
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        /*for (int i = 0;i < arr.length; i++){
            if (arr[i] == num){
                s = i;
                break;
            }
        }
        System.out.println(s);*/

        int index = getIndex(arr, num);
        System.out.println(index);
    }

    public static int getIndex(int[] arr, int num) {
        int s = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                s = i;
                break;
            }
        }
        return s;
    }
}
