package com.java_06;

import java.util.Scanner;

public class Converse {
    public static void main(String[] args) {
        // 创建键盘录入对象
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入字符串：");
        String s = sc.nextLine();

        // 调用方法
        String s1 = con(s);
        System.out.println("s1:" + s1);

    }

    // 字符串反转方法
    public static String con(String s){
        String s1 = "[ ";
        for (int i = s.length()-1 ; i >= 0 ;i--){
            if (i != 0){    // 不是最后一个
                s1 += s.charAt(i) + ", ";

            }else { // 最后一个
                s1 += s.charAt(i);
            }

        }
        s1 += "]";
        // 返回反转后的字符串
        return s1;
    }

}
