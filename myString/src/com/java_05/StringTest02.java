package com.java_05;
/*
    字符串拼接测试类
 */
public class StringTest02 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        StringDemo s = new StringDemo();
        String add = s.andString(arr);
        System.out.println("数组：" + add);

    }
}
