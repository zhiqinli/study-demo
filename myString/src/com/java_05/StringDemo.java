package com.java_05;
/*
    字符串拼接方法
 */
public class StringDemo {

    //int[] arr = {1, 2, 3, 9, 0, 8};

    //String ch;

    public String andString(int[] arr){
        String ch = " ";
        ch += "[";
        for(int i = 0; i < arr.length; i++){
            if (i < arr.length - 1){
                ch += arr[i] + ", ";
            }else {
                ch += arr[i];
            }
        }
        ch += "]";
        return ch;
    }

}
