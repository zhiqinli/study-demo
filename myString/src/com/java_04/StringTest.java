package com.java_04;

import java.util.Scanner;

public class StringTest {
    public static void main(String[] args) {
        // 键盘录入字符串
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入字符串：");
        String s = sc.nextLine();

        // 遍历字符串
        System.out.print("[" + " ");
        for (int i = 0; i < s.length(); i++){
            System.out.print(s.charAt(i) + " ");
        }
        System.out.println("]");
        System.out.println("---------------");

        // 定义统计变量
        int num = 0, bgString = 0, smString = 0;

        // 统计字符次数
        for (int i = 0; i < s.length(); i++){
            char ch = s.charAt(i);
            if (ch >= 'a' && ch <= 'z'){
                smString++;
            }else if(ch >= 'A' && ch <= 'Z'){
                bgString++;
            }else if(ch >= '0' && ch <= '9'){
               num++;
            }
        }
        System.out.println("大写字母：" + bgString +  "\n小写字母：" + smString + "\n数字：" + num);
    }
}
