package com.java_02;

// 字符串的构造方法
public class StringDemo01 {
    public static void main(String[] args) {
        //public string(), 创建一个空白字符串对象，不含有任何内容
        String s1 = new String();
        System.out.println("s1:" + s1); //空白对象

        // public string(char[] chs), 根据字符数组的内容，来创建字符串对象
        char[] chs = {'a', 'b', 'c'};
        String s2 = new String(chs);
        System.out.println("s2:" + s2); // 输出的对象值
        System.out.println(s1 == s2);

        // public string(byte[] bys), 根据字节数组的内容，来创建字符串对象
        byte[] bys = {97, 98, 99};
        String s3 = new String(bys);
        System.out.println("s3:" + s3);

        // String s = "abc",  直接赋值法方式创建字符串对象，内容就是abc
        String s = "abc";
        System.out.println("s:" + s);

        // 比较的引用地址，
        System.out.println(s1 == s2);
        // 比较字符串内容是否相同
        System.out.println(s.equals(s2));
    }
}
