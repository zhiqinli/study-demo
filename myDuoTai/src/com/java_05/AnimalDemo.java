package com.java_05;
/*
    测试类
 */
public class AnimalDemo {
    public static void main(String[] args) {
        // 创建猫类对象进行测试
        Animal a = new Cat();
        a.setName("加菲");
        a.setAge(5);
        System.out.println(a.getName() + a.getAge());
        a.eat();

        a = new Cat("英短",8);
        System.out.println(a.getName() + a.getAge());
        a.eat();

    }


}
