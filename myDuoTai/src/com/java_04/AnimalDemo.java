package com.java_04;
/*
        向上转型：
            从子到父
            父类引用指向子类对象

        向下转型
            从父到子
            父类引用转为子类对象
 */
public class AnimalDemo {
    public static void main(String[] args) {
        // 多态
        Animal a = new Cat();   // 向上转型，父类引用指向子类对象
        a.eat();

        /*//a.platGame();
        // 创建Cat类的对象
        Cat c = new Cat();
        c.eat();
        c.playGame();*/

        // 向下转型,父类引用转为子类对象
        Cat c = (Cat) a;    // 少创建了一个对象，节约了内存
        c.playGame();
        c.eat();

        // 子类狗向上转型为动物
        a = new Gog();
        a.eat();

        // 父类动物引用向下强制转换为子类猫，但是此时父类对象只想狗，所以报错
        Cat cc = (Cat)a;    //ClassCastException
        cc.eat();
        cc.playGame();
    }
}
