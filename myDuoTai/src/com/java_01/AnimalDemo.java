package com.java_01;
/*
    多态：同一个对象，在不同时刻表现出来的不同形态

    多态的前提和体现
        有继承/实现的关系
        有方法重写
        有父类引用指向子类对象
 */

public class AnimalDemo {
    public static void main(String[] args) {
        Animal a = new Cat();
        //Cat c = new Animal();
    }
}
