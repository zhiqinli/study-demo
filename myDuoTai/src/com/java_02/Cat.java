package com.java_02;

public class Cat extends Animal{
    public int age = 100;
    public int weight = 100;

    @Override
    public void eat() {
        System.out.println("猫喜欢吃鱼");
    }

    public void playGame(){
        System.out.println("猫捉老鼠");
    }
}
