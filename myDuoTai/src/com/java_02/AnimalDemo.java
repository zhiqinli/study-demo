package com.java_02;
/*
    测试类
    因为成员方法有重写，变量没有重写
 */
public class AnimalDemo {
    public static void main(String[] args) {
        // 创建的cat对象，使用的animal的地址
        // 内部是猫，但外界看到是动物的引用
        Animal a = new Cat();

        // 运行看左边，访问的动物的age值
        System.out.println(a.age);
        // 编译看左边，动物里面没有weight
        //System.out.println(a.weight);

        //运行的是重写后的成员方法，执行看右边
        a.eat();
        // 编译看左边，动物里面没有playGame方法
        //a.playGame();
    }
}
