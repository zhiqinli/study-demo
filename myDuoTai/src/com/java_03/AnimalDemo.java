package com.java_03;
/*
     测试类
     多态的优势：提高了程序的扩展性
        定义方法采用父类型作为参数，执行操作调用子类型
     多态的弊端：不能使用子类特有的功能
 */
public class AnimalDemo {
    public static void main(String[] args) {
        //创建动物操作类对象，调用方法
        AnimalOperator ao = new AnimalOperator();
        Cat c = new Cat();
        ao.useAnimal(c);

        Dog d = new Dog();
        ao.useAnimal(d);
    }
}
