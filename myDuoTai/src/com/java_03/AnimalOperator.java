package com.java_03;
/*
    动物操作类
 */
public class AnimalOperator {

   /* public void useAnimal(Cat c) {//Cat c = new Cat();
        c.eat();
    }

    public void useAnimal(Dog d) {// Dog d = new Dog();
        d.eat();
    }*/

    public void useAnimal(Animal a){
        // 运行看左边，执行看右边
        // Animal a = new Cat;
        // Animal a = new Dog;
        a.eat();
        // 不能访问子类的特有方法
        //d.loolDog();
    }
}
