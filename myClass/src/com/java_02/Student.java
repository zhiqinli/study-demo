package com.java_02;
/*
    学生类
*/
public class Student {
    // 成员变量
    String name;
    //int age;
    private int age;


    // 方法
    public void setAge(int a){
        if (a > 120 || a < 0){
            System.out.println("你输入的年龄有误");
        }
        else
        age = a;
    }
    // 方法，返回成员变量age
    public int getAge(){
        return age;
    }

    // 成员方法
    public void show(){
        System.out.println(name + "," + age);
    }
}
