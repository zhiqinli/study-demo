package com.java_02;
/*
*   学生测试类
* */

public class StudentDemo {
    public static void main(String[] args) {
        // 创建对象
        Student s = new Student();

        // 给成员变量赋值
        s.setAge(150);
        s.name = "王瑞平";
        System.out.println(s.getAge());
        // 调用show方法
        s.show();
    }
}
