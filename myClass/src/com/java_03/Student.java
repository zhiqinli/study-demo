package com.java_03;
/*
      学生类
 */
public class Student {
    // 成员变量
    private int age;
    private String name;

    // 无参构造
    public Student(){
        System.out.println("无参构造方法");
    }
    // 有参构造
    public Student(String name, int age){
        this.name = name;
        this.age = age;
    }

    // 定义set/get方法
    public void setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return age;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public  void show(){
        System.out.println(name + "，" + age);
    }
}
