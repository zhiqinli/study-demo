package com.java_03;
/*
    学生测试类
 */
public class StudentDemo {
    public static void main(String[] args) {
        // 创建对象
        Student s = new Student();
        Student s1 = new Student("李沁咫",520);

        // 调用s成员变量及方法
        s.setAge(22);
        s.setName("王瑞平");
        s.show();

        // 调用s1成员变量及方法
        s1.show();

        //System.out.println(s.getName() + "---" + s.getAge());

    }
}
