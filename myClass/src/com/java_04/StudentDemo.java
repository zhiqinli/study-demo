package com.java_04;
/*
    学生测试类
 */

public class StudentDemo {
    public static void main(String[] args) {
        // 使用无参构造创建对象
        Student s1 = new Student();
        // 使用有参构造创建函数
        Student s2 = new Student("李沁咫",3);

        // 无参构造函数方法使用set赋值
        s1.setName("王瑞平");
        s1.setGrade(4);
        System.out.println(s1.getName() + "----" + s1.getGrade());

        // 有参构造函数方法使用set赋值
        s2.show();
    }


}
