package com.java_04;
/*
    学生类
 */
public class Student {
    // 成员变量
    private String name;
    private int grade;

    // 无参构造
    public Student(){

    }
    // 有参构造
    public Student(String name, int grade){
        this.name = name;
        this.grade = grade;
    }

    // 成员get、set方法
    public void setGrade(int grade){
        this.grade = grade;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getGrade(){
        return grade;
    }
    public String getName(){
        return name;
    }

    // 成员show方法
    public void show(){
        System.out.println(name + ", " + grade);
    }

}
