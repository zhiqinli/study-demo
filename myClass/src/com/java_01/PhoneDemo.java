package com.java_01;
/*
*   创建对象
*       格式：类名 对象名 = new 类名（）；
*
*   使用对象
*       1.使用成员变量
*           格式：对象名.变量名
*       2.使用成员方法
*           格式：对象名.方法名（）
* */

public class PhoneDemo {
    public static void main(String[] args) {
        // 创建对象
        Phone p = new Phone();  // new出来的在堆

        // 使用对象——成员变量
        System.out.println(p.brand);
        System.out.println(p.price);

        // 改变成员变量的值
        p.brand = "小米";
        p.price = 1999;
        // 重新打印
        System.out.println(p.brand);
        System.out.println(p.price);

        // 使用方法
        p.call();
        p.sendMessage();

        int aa = 10;
        change(aa);
        System.out.println(aa);
    }

    public static void change(int a){
        System.out.println(a);
        a = 100;
        System.out.println(a);
    }

}
