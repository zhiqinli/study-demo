package com.java_01;

public class Phone {
    // 成员变量
    String brand;       // 引用类型，默认值是null
    int price;

    // 成员方法
    public void call(){
        System.out.println(" 打电话");
    }
    public void sendMessage(){
        System.out.println("发短信");
    }


}
