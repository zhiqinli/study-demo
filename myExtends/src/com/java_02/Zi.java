package com.java_02;
/*
    子类会继承父类中的数据，还有可能会使用福来的数据。
    所以，子类初始化之前，一定要先完成父类数据的初始化
    每一个子类构造方法的第一条语句默认都是：super()
 */
public class Zi extends Fu{
    //构造方法
    public Zi(){
        super("liqin");     // 访问父类的代餐构造方法
        // 默认访问父类的无参构造方法
        System.out.println("Zi的无参构造");
    }
    public Zi(String name){
        super("liqin");
        System.out.println("Zi的有参构造");
    }

    // 普通方法
    // 子类调用方法:
    public void method(){
        System.out.println("zi的method方法");
    }


    public void show(){
        super.show();
        System.out.println("zi的show方法");
    }
}
