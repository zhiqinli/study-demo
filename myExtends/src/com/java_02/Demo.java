package com.java_02;
/*
    继承中构造方法、成员方法的访问特点
    子类中所有的构造方法默认都会访问父类中无参构造方法
    子类对象访问成员方法：子类成员范围-》父类成员范围-》都没有则报错（不考虑父亲的父亲。。。）
 */
public class Demo {
    public static void main(String[] args) {
        // 构造方法
        Zi z = new Zi();
        Zi zz = new Zi("liiqnzhi");

        // 普通方法
        z.show();
        z.method();
    }

}
