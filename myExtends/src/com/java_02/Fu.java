package com.java_02;

public class Fu {
    // 构造方法
    /*public Fu(){
        System.out.println("Fu的无参构造");
    }*/
    public Fu(String name){
        System.out.println("Fu的有参构造");
    }

    // 普通方法
    public void show(){
        System.out.println("fu的show方法");
    }
}
