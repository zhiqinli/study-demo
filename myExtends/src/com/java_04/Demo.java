package com.java_04;

public class Demo {
    public static void main(String[] args) {
        // 创建对象
        Teacher t = new Teacher();
        Student s = new Student();


        // 老师1
        t.setAge(22);
        t.setName("王瑞平");
        System.out.println(t.getName() + t.getAge() + "");
        t.teach();

        // 老师2
        // 需要在老师类中定义对应有参构造函数
        Teacher tt = new Teacher(30,"郑朝霞");
        System.out.println(tt.getName() + tt.getAge() + "");
        tt.teach();

        // 学生1
        s.setAge(20);
        s.setName("李沁咫");
        System.out.println(s.getName() + s.getAge() + "");
        s.study();
    }
}
