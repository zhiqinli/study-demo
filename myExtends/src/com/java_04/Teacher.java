package com.java_04;

public class Teacher extends People{

    public Teacher() {
    }

    public Teacher(int age, String name) {
        //this.age = age;  此类中没age，不能这样写
        // 用super调用父类的有参构造函数，传递的参数是子类中的参数
        super(age, name);
    }


    public void teach(){
        System.out.println("老师要教书");
    }
}
