package com.java_01;
/*
    继承中变量访问的访问特点
    在子类中访问一个变量
    子类局部-》子类成员范围-》父类成员范围-》如果都没有就报错（不考虑父亲的父亲）

    super：代表父类存储空间的标识（可以理解为父类对象引用）
    this: 代表本类对象的引用
 */
public class Zi extends Fu {
    public int age = 20;
    int height = 164;

    public void show(){
        int height = 174;
        System.out.println(age);
        System.out.println(height);
        System.out.println("------------");
        // 访问本类的成员变量
        System.out.println(this.height);
        // 访问父类的成员变量
        System.out.println(super.height);
    }
}
