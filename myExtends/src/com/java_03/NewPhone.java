package com.java_03;
/*
    新手机
    java中继承只支持单继承，不支持多继承
    java中继承可以多层继承
 */
public class NewPhone extends Phone{
    // 子类重写父类方法

    @Override       //  @Override 注解可以检查重写方法方法声明的正确性
    public void call(String name){
        System.out.println("开启视频功能");
        //System.out.println("给" + name + "打电话");
        super.call(name);   // 沿袭父类中原有的内容

        // 父类中的私有方法子类不能继承也就不能重写
        /*@Override     重写检验
        private void show(){
            System.out.println("子类的show方法");
        }*/

        //　重写父类方法时，子类的访问权限要大于等于父类
        // public>默认>private
    }
}
