package com.java_03;
/*
    测试类
    方法重写
 */

public class Demo {
    public static void main(String[] args) {
        Phone p  = new Phone();
        p.call("李沁咫");

        NewPhone np = new NewPhone();
        np.call("王瑞平"); // 子类调用父类方法
    }
}
