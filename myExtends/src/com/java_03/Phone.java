package com.java_03;
/*
    手机类
 */
public class Phone {
    public void call(String name){
        System.out.println("给" + name + "打电话");
    }

    private void show(){
        System.out.println("父类的show方法");
    }
}
