package com.java_05;
//import com.java_04.Teacher;

public class Demo extends Animal{
    public static void main(String[] args) {
        // dog无参
        Dog d = new Dog();
        d.setAge(20);
        d.setName("瑞瑞");
        System.out.println( d.getAge() + d.getName());
        d.show();
        // dog有参
        Dog dd = new Dog(5,"亲亲");
        System.out.println( dd.getAge() + dd.getName());
        dd.show();
    }


}
