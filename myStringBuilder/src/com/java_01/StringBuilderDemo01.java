package com.java_01;
/*
    StringBuilder构造方法
 */
public class StringBuilderDemo01 {
    public static void main(String[] args) {
        // public StringBuilder（），创建一个空白可变的字符串对象，不含有任何内容
        StringBuilder sb = new StringBuilder();
        System.out.println("sb:" + sb);
        System.out.println("字符长度：" + sb.length());

        // public StringBuilder（String str），根据字符串的内容来创建可变字符串对象
        StringBuilder sb1 = new StringBuilder("hello");
        System.out.println("sb1:" + sb1);
        System.out.println("字符长度：" + sb1.length());
    }
}
