package com.java_04;
/*
    定义一个方法，使用StringBuilder拼接字符串,相对于String拼接更节省空间
    String本身就是一个对象，因为String不可变对象，所以，每次遍历对字符串做拼接操作，都会重新创建一个对象
    StringBuffer和StringBuilder只需要创建一个StringBuffer或StringBuilder对象，然后用append拼接字符串，只有一个对象
 */
public class StringBuilderDemo04 {
    public static void main(String[] args) {
        // 静态初始化定义一个数组
        int[] arr = {1, 2, 3};
        // 调用拼接字符串方法
        String s = addSB(arr);
        System.out.println("s：" + s);
    }

    // 定义拼接字符串方法
    public static String addSB(int[] arr){
        // 创建StringBuilder对象
        StringBuilder sb = new StringBuilder("[");
        // 使用循环拼接
        for(int i = 0; i < arr.length; i++){
            if (i < arr.length - 1){        // 不是最后一个
                sb.append(arr[i]).append(", ");
            }else {
                sb.append(arr[i]).append("]");
            }
        }
        // 将StringBuilder转换为String
        String s = sb.toString();

        return s;
    }
}
