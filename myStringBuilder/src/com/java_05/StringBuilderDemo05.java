package com.java_05;

import java.util.Scanner;

/*
    使用StringBuilder定义方法实现字符串反转
 */
public class StringBuilderDemo05 {
    public static void main(String[] args) {
        // 创建键盘录入对象
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入字符串：");
        String s = sc.nextLine();

        // 调用字符串反转方法
        String s1 = reverse(s);
        System.out.println("s1:" + s1);
    }
    // 定义字符串反转方法
    public static String reverse(String s){
        StringBuilder sb = new StringBuilder(s);
        s = sb.reverse().toString();    //先使用sb的反转函数，返回反转后的对象，再对其使用转换函数，返回String类型对象
        return s;
        //return new StringBuilder(s).reverse().toString();
    }


}
