package com.java_03;
/*
    StringBuilder和String的相互转换
 */
public class StringBuilderDemo03 {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        sb.append("hello");
        // public String toString(), 把StringBuilder转换为String，toString是StringBuilder的一个方法，返回的String类型
        //String s = sb; 错误，因为s和sb 是两个数据类型，不能直接赋值转换
        String s = sb.toString();
        System.out.println("s:" + s);

        // public StringBuilder（String s），通过构造方法把String转换为StringBuilder
        //StringBuilder sb2 = s;错误，因为s和sb2 是两个数据类型，不能直接赋值转换
        StringBuilder sb2 = new StringBuilder(s);
        System.out.println("sb2:" + sb2);
        System.out.println(sb == sb2);
    }
}
