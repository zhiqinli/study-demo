package com.java_02;
/*
    StringBuilder 的添加和反转方法
 */
public class StringBuilderDemo02 {
    public static void main(String[] args) {
        // 创建对象
        StringBuilder sb = new StringBuilder();
        // public StringBuilder append(任意类型)，添加数据，“并返回对象本身”
        System.out.println("sb:" + sb); // 还未赋值，所以是空
        StringBuilder s1 = sb.append("wangruiping");    //因为返回的对象本身，所以s1实际上和s同地址
        System.out.println(s1 == sb);

        // 链式编程
        sb.append("520").append("liqinzhi");

        System.out.println(s1);
        s1.reverse();
        System.out.println(sb);
        // public StringBuilder reverse();返回相反的字符序列


    }
}
