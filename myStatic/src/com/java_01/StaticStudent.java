package com.java_01;

public class StaticStudent {
    public static void main(String[] args) {
        //静态修饰的成员可以通过对象名访问，也可以通过类名访问
        //Student.univercity  = "四川师范大学";

        Student s = new Student();
        s.name = "李沁咫";
        s.age = 20;
        //静态修饰的成员可以通过对象名访问，也可以通过类名访问
        //s.univercity = "四川师范大学";
        s.show();

        Student ss = new Student();
        ss.name = "任颖";
        ss.age = 21;
        //ss.univercity = "四川师范大学";
        ss.show();

    }
}
