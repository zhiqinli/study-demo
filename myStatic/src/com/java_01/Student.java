package com.java_01;

public class Student {
    public int age;
    public String name;
    // static可以修饰成员方法或者成员变量
    //用static修饰，表示被所有对象共享
    public static String univercity;

    public void show(){
        System.out.println(name + " " + age + " " + univercity);
    }
}
