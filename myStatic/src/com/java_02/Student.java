package com.java_02;
/*
    static访问特点
 */

public class Student {
    // 非静态变量
    private String name = "李沁咫";
    // 静态变量
    private static String univercity = "四川师范大学";
    // 非静态方法


   //非静态方法,可以访问静态、非静态的成员及方法
    public void show1(){
        System.out.println(name);
        System.out.println(univercity);
        show2();
        show4();

    }
    public void show2(){

    }


    // 静态成员方法，只能访问静态的成员及方法

    public static void show3(){
       // System.out.println(name);
        System.out.println(univercity);
        //show2();
        show4();
    }

    public static void show4(){

    }


    public static void main(String[] args) {

    }
}
