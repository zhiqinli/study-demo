package com.java_06;

public class MethodTest6 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 6, 8, 7};
        System.out.println( maxArray(arr));
    }

    public static int maxArray(int[] arr){
        int max = arr[0];
        for (int i = 1; i < arr.length; i++){
            max = (max > arr[i] ? max : arr[i]);
        }
        return max;
    }
}
