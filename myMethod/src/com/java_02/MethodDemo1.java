package com.java_02;
import java.util.Scanner;

public class MethodDemo1 {
    public static void main(String[] args) {
        // 创建对象
        Scanner sc = new Scanner(System.in);

        // 接收数据
        System.out.print("请输入第一个数");
        int x = sc.nextInt();
        System.out.print("请输入第二个数");
        int y = sc.nextInt();
        System.out.println("最大值为：" + getMax(x, y));

/*
        float f = getMax(1.2f,20.6f);
        System.out.println(f);
        System.out.println(getMax(20.5f,56.2f));
*/
    }
    // 定义一个方法
    public static int getMax(int x, int y){
        return ((x > y ? x : y));
    }
}
