package com.java_05;

    // println换行；print不换行
public class MethodTest01 {
    public static void main(String[] args) {
        int[] a = {1, 55, 66, 89, 23};
        myArray(a);
    }

    public static void myArray(int[] arr){
        for (int i = 0;i < arr.length; i++){
            System.out.print(arr[i] + "  ");
        }
    }
}
