package com.java_03;

public class MethodDemo3 {
    public static void main(String[] args) {
        System.out.println(myMethod(1,2));
        System.out.println(myMethod(23.2,563.6));
        System.out.println(myMethod(1, 2, 3));

    }

    // 方法重载与返回值无关，
    // 在调用的时候虚拟机会通过擦书的不同来区分同名的方法
    public static int myMethod(int x, int y){
        return x;
    }
    public static double myMethod(double x, double y){
        return x;
    }
    public static int myMethod(int x,int y, int z){
        return x;
    }

}
