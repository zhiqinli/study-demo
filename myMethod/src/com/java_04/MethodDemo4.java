package com.java_04;

    // 对于基本参数类型的参数，形式参数的改变，不影响实际参数的值
    // 对于引用参数类型的参数，形式参数的改变，影像实际参数的值
public class MethodDemo4 {
    public static void main(String[] args) {
/*        int num = 100;
        System.out.println("调用方法前" +num);
        change(num);
        System.out.println("调用方法后" +num);*/

        int[] arr = {1, 2, 3};
        System.out.println("调用方法前" + arr[1]);
        change(arr);
        System.out.println("调用方法后" + arr[1]);
    }

    public static void change(int s){
        s = 200;
    }
    public static void change(int[] a){
        a[1] = 200;
    }
}
