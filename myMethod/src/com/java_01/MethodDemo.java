package com.java_01;

public class MethodDemo {
    public static void main(String[] args) {
        System.out.println(assertion(5));
    }
    // 需求：定义一个方法，在方法中定义一个变量，判断该数据是否是偶数
    public static boolean assertion(int x){
        if (x % 2 == 0)
            return true;
        else
            return false;
    }
}
