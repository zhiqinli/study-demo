package com.java_01;

public class FinalDemo {
    public static void main(String[] args) {
        // final修饰基本类型变量,
        final int age = 20;
        //age = 100;             数据值不可再修改
        System.out.println(age);

        // final 修饰引用类型变量
        final Student s = new Student();    // 修饰的是s的地址，
        s.age = 100;            // 地址里面的内容可以改变
        System.out.println(age);

        //s = new Student();    报错，地址值不能发生改变
    }
}
