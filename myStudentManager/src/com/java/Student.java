package com.java;
/*
        学生类
        Alt insert根据需要自动生成构造get set方法
 */
public class Student {
    // 成员
    private String name;
    private String age;
    private String address;
    private String sid;

    // 构造方法 alt insert快速生成
    public Student() {
    }

    public Student(String name, String age, String address, String sid) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.sid = sid;
    }

    //获取成员变量的方法  alt insert快速生成
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }




}
