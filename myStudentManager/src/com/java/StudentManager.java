package com.java;

import java.util.Scanner;

/*
        学生管理系统主界面
 */
public class StudentManager {
    public static void main(String[] args) {

        // 用Scanner完成键盘录入数据
        Scanner sc = new Scanner(System.in);
        int num;

        // 创建学生管理方法类
        StudentManagerMethod smm = new StudentManagerMethod();

        // 用循环再次回到主界面
        while (true) {
            // 用输出语句完成主界面的编写
            System.out.println("----------欢迎来到学生管理系统----------");
            System.out.println("1 添加学生");
            System.out.println("2 删除学生");
            System.out.println("3 修改学生");
            System.out.println("4 查看所有学生");
            System.out.println("5 退出");
            System.out.print("请输入你的选择：");

            num = sc.nextInt();

            // 用switch语句完成操作的选择
            switch (num) {
                case 1:
                    //System.out.println("1 添加学生");
                    smm.addStudent();
                    break;
                case 2:
                    //System.out.println("2 删除学生");
                    smm.delStudent();
                    break;
                case 3:
                    //System.out.println("3 修改学生");
                    smm.altStudent();

                    break;
                case 4:
                    //System.out.println("4 查看所有学生");
                    smm.showStudent();
                    break;
                case 5:
                    System.out.println("*********谢谢使用*********");
                    System.exit(0); // JVM退出    java虚拟机退出

                default:
                    System.out.println("你的选择有误");
                    break;
            }
        }

    }

}
