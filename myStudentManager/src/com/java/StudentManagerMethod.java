package com.java;

import java.util.ArrayList;
import java.util.Scanner;

public class StudentManagerMethod {
    // 定义集合和键盘录入对象
    ArrayList<Student> arr = new ArrayList<>();
    Scanner sc = new Scanner(System.in);

    // 方法： 判断是否有相同学号
    public boolean compStudent(String sid){
        boolean flag = false;
        for (int i = 0; i < arr.size(); i++){
            Student s = arr.get(i);
            if (s.getSid().equals(sid)){        // 学号存在
                flag = true;
                break;      // 结束遍历
            }
        }
        return flag;
    }

    // 方法：添加学生信息
    public void addStudent() {
        String sid;
        while (true) {
            System.out.print("请输入学号：");
            sid = sc.nextLine();

            if (compStudent(sid))   // flag为true   即学号存在
            {
                System.out.println("该学号已存在");
            }else break;        // 学号不存在
        }

        System.out.print("请输入姓名：");
        String name = sc.nextLine();
        System.out.print("请输入地址：");
        String address = sc.nextLine();
        System.out.print("请输入年龄：");
        String age = sc.nextLine();

        Student s = new Student(name, age, address, sid);

        // 将学生对象添加到集合中，返回是否添加成功
        if (arr.add(s))
            System.out.println("添加学生成功！");
        else
            System.out.println("添加学生失败！");

    }

    // 方法：查看所有学生信息
    public void showStudent(){
        if (arr.size() == 0){
            System.out.println("无信息，请添加信息!");
            // 程序不再往下执行
            return;
        }

        // 表头           \t  tab
        System.out.println("学号\t\t姓名\t\t\t年龄\t\t居住地");

        // 循环遍历输出学生对象的各个属性
       for (int i = 0; i < arr.size();i++){
           Student s = arr.get(i);
           System.out.println(s.getSid() + "\t\t" + s.getName() + "\t\t\t"
                   + s.getAge() + "岁" + "\t\t" + s.getAddress());
       }
    }

    // 方法： 删除学生信息
    public void delStudent(){
        int index = -1;

        // 如果集合为空，提示
        if (arr.size() == 0){
            System.out.println("目前没有学生信息，无法删除！");
            return;
        }

        System.out.print("请输入你要删除的学生的学号：");
        String delSid = sc.nextLine();

        // 遍历集合删除指定学生对象
        for (int i = 0; i < arr.size(); i++){
            Student s = arr.get(i);
             // 因为字符串之间比较的是地址，所以需要用equals方法
            if (delSid.equals(s.getSid())){    // 有此学号
                arr.remove(s);
                index = i;
                break;      // 跳出for循环
            }
        }

        if (index == -1)
            System.out.println("集合中没有该对象！");
        else
            System.out.println("删除成功！");
    }

    // 方法： 修改学生信息
    public void altStudent(){
        int index = -1;
        // 如果集合为空，提示
        if (arr.size() == 0){
            System.out.println("目前没有学生信息，无法删除！");
            return;
        }

        // 键盘录入修改学生的学号
        System.out.print("请输入要修改的学生的学号：");
        String altSid = sc.nextLine();

        // 遍历集合修改学生信息
        for (int i = 0; i < arr.size(); i++){

            Student s = arr.get(i);
            if (s.getSid().equals(altSid)){     // 学号对应学生存在
                // 姓名
                System.out.print("请输入学生新姓名：");
                s.setName(sc.nextLine());
                // 学号
                System.out.print("请输入学生新学号：");
                s.setSid(sc.nextLine());
                // 地址
                System.out.print("请输入学生新地址：");
                s.setAddress(sc.nextLine());
                // 年龄
                System.out.print("请输入学生新年龄：");
                s.setAge(sc.nextLine());

                index = i;
            }
        }
        //
        if (index == -1){
            System.out.println("没有该学生！");
        }else
            System.out.println("修改成功");
     }
}
